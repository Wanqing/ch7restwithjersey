package jerseycustomer;

import java.util.ArrayList;
import java.util.List;

public class CustomerDao {
	public List<Customer> getAllCustomers() {
		List<Customer> customerList = null;
		Customer customer_1 = new Customer(1, "John Jones", "1234567890", "1355 S Hines Blvd");
		Customer customer_2 = new Customer(2, "David Smith", "9876543210", "1900 Allard Ave");
		customerList = new ArrayList<Customer>();
		customerList.add(customer_1);
		customerList.add(customer_2);

		return customerList;
	}
}
